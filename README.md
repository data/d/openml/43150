# OpenML dataset: ALARM_dataset

https://www.openml.org/d/43150

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Dataset description**

The ALARM ("A Logical Alarm Reduction Mechanism") is a Bayesian network designed to provide an alarm message system for patient monitoring.

The alarm data set contains the following 37 variables:

CVP (central venous pressure): a three-level factor with levels LOW, NORMAL and HIGH.

PCWP (pulmonary capillary wedge pressure): a three-level factor with levels LOW, NORMAL and HIGH.

HIST (history): a two-level factor with levels TRUE and FALSE.

TPR (total peripheral resistance): a three-level factor with levels LOW, NORMAL and HIGH.

BP (blood pressure): a three-level factor with levels LOW, NORMAL and HIGH.

CO (cardiac output): a three-level factor with levels LOW, NORMAL and HIGH.

HRBP (heart rate / blood pressure): a three-level factor with levels LOW, NORMAL and HIGH.

HREK (heart rate measured by an EKG monitor): a three-level factor with levels LOW, NORMAL and HIGH.

HRSA (heart rate / oxygen saturation): a three-level factor with levels LOW, NORMAL and HIGH.

PAP (pulmonary artery pressure): a three-level factor with levels LOW, NORMAL and HIGH.

SAO2 (arterial oxygen saturation): a three-level factor with levels LOW, NORMAL and HIGH.

FIO2 (fraction of inspired oxygen): a two-level factor with levels LOW and NORMAL.

PRSS (breathing pressure): a four-level factor with levels ZERO, LOW, NORMAL and HIGH.

ECO2 (expelled CO2): a four-level factor with levels ZERO, LOW, NORMAL and HIGH.

MINV (minimum volume): a four-level factor with levels ZERO, LOW, NORMAL and HIGH.

MVS (minimum volume set): a three-level factor with levels LOW, NORMAL and HIGH.

HYP (hypovolemia): a two-level factor with levels TRUE and FALSE.

LVF (left ventricular failure): a two-level factor with levels TRUE and FALSE.

APL (anaphylaxis): a two-level factor with levels TRUE and FALSE.

ANES (insufficient anesthesia/analgesia): a two-level factor with levels TRUE and FALSE.

PMB (pulmonary embolus): a two-level factor with levels TRUE and FALSE.

INT (intubation): a three-level factor with levels NORMAL, ESOPHAGEAL and ONESIDED.

KINK (kinked tube): a two-level factor with levels TRUE and FALSE.

DISC (disconnection): a two-level factor with levels TRUE and FALSE.

LVV (left ventricular end-diastolic volume): a three-level factor with levels LOW, NORMAL and HIGH.

STKV (stroke volume): a three-level factor with levels LOW, NORMAL and HIGH.

CCHL (catecholamine): a two-level factor with levels NORMAL and HIGH.

ERLO (error low output): a two-level factor with levels TRUE and FALSE.

HR (heart rate): a three-level factor with levels LOW, NORMAL and HIGH.

ERCA (electrocauter): a two-level factor with levels TRUE and FALSE.

SHNT (shunt): a two-level factor with levels NORMAL and HIGH.

PVS (pulmonary venous oxygen saturation): a three-level factor with levels LOW, NORMAL and HIGH.

ACO2 (arterial CO2): a three-level factor with levels LOW, NORMAL and HIGH.

VALV (pulmonary alveoli ventilation): a four-level factor with levels ZERO, LOW, NORMAL and HIGH.

VLNG (lung ventilation): a four-level factor with levels ZERO, LOW, NORMAL and HIGH.

VTUB (ventilation tube): a four-level factor with levels ZERO, LOW, NORMAL and HIGH.

VMCH (ventilation machine): a four-level factor with levels ZERO, LOW, NORMAL and HIGH

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43150) of an [OpenML dataset](https://www.openml.org/d/43150). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43150/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43150/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43150/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

